/* Program description*/
/*----------------------
Program: Degrees to radians
Programmer  
Student ID: 
Date: 22.09.17
Purpose: Go trough all degrees from 0 to 360 and show how many radians they are.
-------------------------*/


#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    const float pi = 3.14 ;
    float rad = 0 ;
    int deg = 0 ;

    while( deg < 360) {
    deg ++ ;
    rad =  deg * ( pi / 180 ) ;
    printf("%d : degrees is : %.2f Radians\n", deg, rad ) ;
    }
    return 0 ;
}
