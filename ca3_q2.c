/* Program description*/
/*----------------------
Program: Body mass index calculator
Programmer  
Student ID: 
Date: 22.09.17
Purpose: Asks for users weight and heigt then shows users what BMI he/she is.
-------------------------*/


#include <stdio.h>
#include <stdlib.h>

int main(void) {
    // Declare variables for all we need
    int weight = 0, bmi = 0 ;
    float height = 0 ;

    //Print to user what we are about to do and ask the user height.
    printf("Hello, Let's see what kind of BMI you have.\nPlease enter you height in meters : \n" ) ;
    scanf("%f", &height ) ;

    //Ask user weight.
    printf("Then please enter your weight in kilograms : \n" ) ;
    scanf("%d",&weight);

    //Assign the BMI caluclation to bmi variable
    bmi = weight / ( height * height )  ;

    // start if loop
    if( bmi < 18.5 ) {
    printf("Your BMI is %d  which is under 18.5. You're underwighted.\n" , bmi);
    }

    // else if notice greater than equal there is no gaps
    else if ( bmi >= 18.5 && bmi < 24.9 )   {
    printf("Your bmi is %d which is normal\n" , bmi ) ;
    }

    else if ( bmi >= 25 && bmi < 29.9 ) {
    printf("Your BMI is %d and that means you're overweigth\n" , bmi ) ;
    }

    else if (bmi >= 30 )  {
    printf("Your BMI is %d meaning that you are obese" , bmi ) ;
    }

    //Finally do nothing or return to null
    else( NULL ) ;
    return 0 ;
}
