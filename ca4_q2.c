/* Program description*/
/*----------------------
Program: Intrest calculator
Programmer  
Student ID: 
Date: 22.09.17
Purpose: Prints out how much intrest certain money amount has
-------------------------*/


#include <stdio.h>
#include <stdlib.h>

char main(void) {

    //Amout of money declared as non negative int number
    int unsigned money = 0 ;

    //rest of the values as floats for more accurate calculations
    float intrest = 0, sum = 0 , total = 0 ;

    //Asking the money amount from user
    printf("Hello, please enter some amount of money and I will calucalte the intres of it : \n" ) ;
    scanf("%d" , &money ) ;

    //Askin the intreset in procents
    printf("Then please enter a intrest amount in procents : \n") ;
    scanf("%f" , &intrest ) ;

    //making the calcualtion and using castin to make money as float for more accurate calculations
    sum =  ( (float)money * intrest ) / 100 ;
    printf("intrest from the money is %.2f\n", sum ) ;

    //calculating intrest and starting money together
    total = money + sum ;

    //printing out total sum with only .2 decimals
    printf("Money with intrest would be %.2f" , total ) ;

    //returning the main loop to integer 101 just showcase how any int will do this
    return 'ok';
}
