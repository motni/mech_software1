/* Program description*/
/*----------------------
Program: Grade number to letter
Programmer  
Student ID: 
Date: 21.10.17
Purpose: Print out students number grade as number letter
-------------------------*/


#include <stdio.h>
#include <stdlib.h>

int main()
{
    //variable for the grade
    int unsigned number = 0 ;

    //asking the user to enter a grade
    printf("Hello please enter you grade number and program will show you the letter for the grade \n");
    printf("Please enter a number : ") ;
    scanf("%d", &number ) ;

    //starting from bottom if under 40 marks
    if(number < 40 )
    printf("Your grade is F\n" ) ;

    //Going forward in in ifelse chain and usin >= 40 include or bigger
    else if(number >= 80 ) {
    printf("Your grade is A\n" ) ;
    }
    //going down in the chain using >=
    else if(number >= 70 ) {
    printf("Your grade is B\n" ) ;
    }
    else if(number >= 50 ) {
    printf("Your grade is C\n" ) ;
    }
    else  {
    printf("Your grade is D\n" ) ;
    }



    return 0;
}
