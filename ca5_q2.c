/* Program description*/
/*----------------------
Program: Triange calulator
Programmer  
Student ID: 
Date: 22.09.17
Purpose: This will calculate the unknown side of a triangle with 2 sides and angel
-------------------------*/

#include <stdio.h>
#include <stdlib.h>
#define PI 3.142
#include <math.h>
int main()
{
    //declaring couple integer to make the calcualtions
    int signed sidea = 0, sideb = 0, sidec = 0, angledeg = 0 ;

    //declaring float for radians
    float anglerad = 0 ;

    printf("Hello, Let's solve of on the triangels sides\n" ) ;
    printf("Please enter the 2 sides of the triangle. Hit enter after you entered the number\n" ) ;
    scanf("%d", &sideb ) ;
    scanf("%d", &sidec ) ;

    printf("Then please enter the degrees of one known angle : \n" ) ;
    scanf("%d" , &angledeg ) ;

    //making degrees to rads
    anglerad = (PI / 180) * (float)angledeg ;

    //print it out to angles in degrees and rads to see it went ok
    printf("Angle in rads is : %f and in degrees : %d \n" ,anglerad, angledeg ) ;

    //Triangel solving calcualtions
    sidea = sqrt( (sideb * sideb ) + (sidec * sidec ) - (2 * sideb * sidec * cos(anglerad)) );

    //print out the answer
    printf("The length of the unknown side is : %d" , sidea ) ;

    return 0;
}
