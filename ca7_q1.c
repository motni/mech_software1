/* Program description*/
/*----------------------
Program: Calculate the harmonics
Programmer  
Student ID: 
Date: 09.11.17
Purpose: Ask user how many harmonics is needed then calculate the sum.
-------------------------*/



#include <stdio.h>
#include <stdlib.h>

int main()
{

    int n = 0;
    double sum = 0;
    double answer = 0 ;
    //start the i from to Eg 1/2 then add up the +1 later on
    float i = 2 ;

    //ask the N from user
    printf("Hello world, let's calculate harmonics!\n");
    printf("Please enter the n number of harmonics you want to be calculated : \n");
    scanf("%d", &n ) ;

    //loop it n times. i here track how many loops is deon
    while(i <= n ) {
    answer += 1/i ;
    i ++ ;
    }

    //Print the answer and add the fundamental harmonic +1 in there.
    printf("%lf", answer + 1 ) ;

    return 1;
}
