/* Program description*/
/*----------------------
Program The fiwer adder
Programmer  
Student ID: 
Date: 22.09.17
Purpose: this program will ask 5 numbers then sum them.
-------------------------*/



#include <stdio.h>
#include <stdlib.h>

int main()
{
    int integ1, integ2, integ3, integ4, integ5;

    printf("Hello, I'm going to ask 5 numbers from you and then sum them.\n");
    printf("Please enter the first number and hit enter : ");
    scanf("%d", &integ1);
    printf("you did enter %d\n",integ1);

    printf("Please enter the second number and hit enter : ");
    scanf("%d", &integ2);
    printf("you did enter %d\n",integ2);

    printf("Please enter the third number and hit enter : ");
    scanf("%d", &integ3);
    printf("you did enter %d\n",integ3);

    printf("Please enter the fourth number and hit enter :");
    scanf("%d", &integ4);
    printf("you did enter %d\n",integ4);

    printf("Please enter the fifth number and hit enter : ");
    scanf("%d", &integ5);
    printf("you did enter %d\n",integ5);

    int sum = integ1+integ2+integ3+integ4+integ5;
    printf("the sum of numbers is : %d \n",sum);


        return 0;
}
