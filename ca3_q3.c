/* Program description*/
/*----------------------
Program: While loop printer
Programmer  
Student ID: 
Date: 22.09.17
Purpose: Print out 10 numbers from while loop and print extra if number is even
-------------------------*/



#include <stdio.h>
#include <stdlib.h>

int main()
{   //Registered integer to be used registered is just fancy not needed for this
    register int i = 0;

    //start while loop with condition
    while( i <= 9 ) {

    //But the i in loop to as a way to calculate how many times it goes around
    i ++ ;

    //prtint out the i number with message
    printf("%d\tI do like while looping\n", i  ) ;

    //if statement to the loop notice this is inside the same curly brackets
    if( i % 2 == 0  )

    //Prtin out message if the if statement is valid
    printf("%d\tThis is even number\n", i ) ;
    }

    //Starting the B section of the question
    //return to zero
    i = 0 ;
    printf("\n\nThen let's to some for loopin!\n" ) ;
    for(i = 1; i <= 10 ; i++ ) {
    printf("%d\tfor loops are easier to use!\n", i ) ;
    if( i % 2 == 1 )
    printf("%d\tAnd this is odd number\n", i) ;
    }
    return 0;
}
