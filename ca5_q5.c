/* Program description*/
/*----------------------
Program: ASCII art cube
Programmer  
Student ID: 
Date: 22.10.17
Purpose: Print ascii art cube made of *
-------------------------*/



#include <stdio.h>
#include <stdlib.h>

int main()
{
    //Declare short int as number we use is under 20
    unsigned short int x = 0, i = 0, side = 0 ;

    //Print what are we doing and ask number from user
    printf("Hello lets print out a ASCII art cube that sides are smaller than 20 marks. \n") ;
    printf("Please enter how many marks the sides should be : \n" ) ;
    scanf("%hu", &side ) ;

    //Print out the number we got and add couple new lines
    printf("\nYou did enter : %hu \n \n", side  ) ;

    //Checking that number is smaller than 20
    while(side > 20 ) {
    printf("Error numbers is bigger than 20 and can't be printed out \n" )  ;
    return 'ERROR' ;
    }
    //If number is smalle than 20 go ahed with the rest of its
    while(side <  20 ) {
    printf("Ok the number is smaller than 20 we can print out the cube\n\n" ) ;

    //first while loop foreturn 0;r the up to down side
    for(i = 1 ; i < side ; i++ ) {

        //second while loop for sideways counting notice the curly brackets
        for(x = 1 ; x < side ; x++ ) {
        printf("*");
    //This curly bracket closes the sideways counting
    }
    printf("\n");
    //This curly bracket closes the up to down counting
    }
    //return while loop that checked if smaller than 20 to ok state
    return 0;
    //this curly bracket closes the while loop off
    }
    //main loop ok
    return 0;
}
