/* Program description*/
/*----------------------
Program: Printing weekdays and dates
Programmer  
Student ID: 
Date: 22.09.17
Purpose: Print simple calendar to console
-------------------------*/

#include <stdio.h>
#include <stdlib.h>

int main()
{
    printf("Sun\t Mon\t Tue\t Wed\t Thu\t Fri\t Sat\t\n");
    printf("\t \t \t \t 1\t 2\t 3\n");
    printf("4\t 5\t 6\t 7\t 8\t 9\t 10\n");
    printf("11\t 12\t 13\t 14\t 15\t 16\t 17\n");
    printf("18\t 19\t 20\t 21\t 22\t 23\t 24\n");
    printf("25\t 26\t 27\t 28\t 28\t 30\t 31\n");
    return 0;
}
