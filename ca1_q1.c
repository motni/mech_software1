/* Program description*/
/*----------------------
Program: Hello world console print.
Programmer 
Student ID: 
Date: 22.09.17
Purpose: Print hello world with student details to console.
-------------------------*/

#include <stdio.h>
#include <stdlib.h>

int main()
{
    printf("Hello world! My name is foo bar and my student ID is foo\n");
    return 0;
}
