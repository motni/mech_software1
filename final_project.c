#include <stdio.h>
#include <stdlib.h>
#include <math.h>

//Declare couple functions we are going to use in this
float torads(int ) ;
float sigmaone(float, float, float  ) ;
float sigmatwo(float, float, float  ) ;



    //This function will convert angles to radians
    float torads(int angle) {
        float rads = (angle * M_PI / 180.0) ;
        return rads ;
    }

    //sigmaone function here the squared part is plus
    float sigmaone(float insigmax, float insigmay, float intauxy ) {

        //making the calcualtion in parts
        float results1 = ( insigmax + insigmay ) / 2 ;
        float results2 = pow( (insigmax - insigmay / 2), 2 ) + pow(intauxy, 2 ) ;
        float results3 = results1 + sqrt(results2) ;

        return results3 ;
    }

    //sigmatwo function where the squared part is minus
    float sigmatwo(float insigmax, float insigmay, float intauxy ) {

        float results1 = ( insigmax + insigmay ) / 2 ;
        float results2 = pow( (insigmax - insigmay / 2), 2 ) + pow(intauxy, 2 ) ;
        float results3 = results1 - sqrt(results2) ;

        return results3 ;
    }




int main()
{



    //pointer to a open file
    FILE *resultsfile ;


    //Assign variable name to the open file w is writing and overwriting the old data in text file
    resultsfile = fopen("results.txt", "w") ;

    //Check that we can open the file in write mode
    if(resultsfile != NULL ) {
    printf("\nAll good file is open for writing. \n");




    //Declare few variables
    float insigmax = 0, insigmay = 0, intauxy = 0, results1 = 0 , results2 = 0 ;
    int i = 0, k = 0 ;
    //The important results array where results will be stored
    float resultsarray[35][2] ;


    //Ask the input values from user
    printf("Hello please give the three values. sigma x, sigma y and tauxy :  ") ;
    scanf("%f" , &insigmax ) ;
    scanf("%f" , &insigmay ) ;
    scanf("%f" , &intauxy ) ;

    //Write header or bit information to beginning of the text file
    fprintf(resultsfile, "\n User did enter sigma x as : %f \n sigma y as : %f \n tauxy was : %f \n\n", insigmax, insigmay, intauxy ) ;


    //Return the values to zero before new calcualtions just in case.
    results1 = 0 ;
    results2 = 0 ;

    //Calucate the first part for sigma x
    results1 = (( insigmax + insigmay ) / 2 ) + (( insigmax - insigmay ) / 2 ) ;

    //calculate the second part for simga x and loop in the angle as radians using the function. Print them out and write to array
    for(i = 0 ; i <= 360 ; i += 10 ) {
        printf("Angle is %d and in rads %f ",  i, torads(i)) ;
        results2 = results1 * cos(2 * torads(i)) + intauxy * (sin(2 * torads(i) )) ;
        k = 0 ;
        //print all out and in array and to file
        resultsarray[k][0] = results2 ;
        printf("sigma x is  : %.2f\n" , results2 ) ;
        printf("Hello from the arrays! : %f \n" , resultsarray[k][0]) ;
        fprintf(resultsfile, "sixma x is is : %f \n", resultsarray[k][0] );
        k ++ ;
    }

    //Return the values to zero before new calcualtions just in case.
    results1 = 0 ;
    results2 = 0 ;

    //Calcualte the first part of sigma y
    results1 = (( insigmax + insigmay ) / 2 ) - (( insigmax - insigmay ) / 2 ) ;

    //Same as previous but for sigma y
    for(i = 0 ; i <= 360 ; i += 10 ) {
        printf("Angle is %d and in rads %f ",  i, torads(i)) ;
        results2 = results1 * cos(2 * torads(i)) - intauxy * (sin(2 * torads(i) )) ;
        k = 0 ;
        //print all out and in array and to file
        resultsarray[k][1] = results2 ;
        printf("sigma y is  : %.2f\n" , results2 ) ;
        printf("Hello from the arrays! : %f \n" , resultsarray[k][1]) ;
        fprintf(resultsfile, "sixma y is is : %f \n", resultsarray[k][1] );
        k ++ ;
    }

    //Return the values to zero before new calcualtions just in case.
    results1 = 0 ;
    results2 = 0 ;

    //Calcualte the first part of tau xy
    results1 =  ( (insigmax - insigmay) / 2) * -1   ;

    //Same as previous but for tauxy
    for(i = 0 ; i <= 360 ; i += 10 ) {
        printf("Angle is %d and in rads %f ",  i, torads(i)) ;
        results2 = results1 * (sin(2 * torads(i) ) ) + (intauxy * cos(2 * torads(i) ) ) ;
        k = 0 ;
        //print all out and in array and to file
        resultsarray[k][2] = results2 ;
        printf("tau xy  is  : %.2f\n" , results2 ) ;
        printf("Hello from the arrays! : %f \n" , resultsarray[k][2]) ;
        fprintf(resultsfile, "tauxy is : %f \n", resultsarray[k][2] );
        k ++ ;
    }


    //print out the sigam one and sigma 2
    printf("\nSigma one + is : %f ", sigmaone(insigmax, insigmay, intauxy ) ) ;
    printf("\nSigma one - is : %f ", sigmatwo(insigmax, insigmay, intauxy ) ) ;




        //print out the sigam one and sigma 2 to text file
        fprintf(resultsfile, "\nSigma one + is : %f ", sigmaone(insigmax, insigmay, intauxy ) ) ;
        fprintf(resultsfile, "\nSigma two - is : %f \n", sigmatwo(insigmax, insigmay, intauxy ) ) ;


        fclose(resultsfile ) ;

        }


    //if we can't open the file print out error
    else  {
    printf("Error can't open the file. \n") ;
    }



   return 0;

}
