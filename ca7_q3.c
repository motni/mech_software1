/* Program description*/
/*----------------------
Program: Consonants checker
Programmer  
Student ID: 
Date: 12.11.17
Purpose: Get 10 charter from the user and show how many upper, lower and consonant charters there is.
-------------------------*/



#include <stdio.h>
#include <stdlib.h>

int main() {

    //Array for the 10 charters
    char word[50] ;
    //Short integers to calcualte how many upper or lower letters we get
    int unsigned lower = 0 ;
    int unsigned upper = 0 ;
    //int to see how many consonants there is
    int unsigned cons = 0 ;
    //Variable to be used in loops
    int unsigned j = 0 ;
    int unsigned n = 0 ;
    //Couple arrays that have the lower and upper consonants
    char lcon[] = {'a', 'e', 'i', 'o', 'u' } ;
    char ucon[] = {'A', 'E', 'I', 'O', 'U' } ;


    //start by askin the charters from user
    printf("Hello world: let's see how many consonants you will write.\n");
    printf("Please enter 10 letters\n") ;

    //Scan charters in by looping
    for(j = 0 ; j <= 10 ; j ++ ) {
    scanf("%c", &word[j]) ;
    }

    //show them to user
    printf("You entered the following word :\n" ) ;
    for(j = 0 ; j <= 10 ; j ++ ) {
    printf("%c" , word[j]) ;
    }

    /*Check if letters are upper or lower. Method is taken from here:
    http://www.c4learn.com/c-programs/check-whether-character-is-uppercase-or.html
    */

    //Looping number to go trough array
    for(j = 0 ; j <= 10 ; j ++ ) {
        //If charter in array matches the 'charter'
        if(word[j] >= 'A' && word[j] <= 'Z' ) {
        upper ++ ;
        }
        //use bit math to get the lower cases
        else
        lower = 10 - upper ;
    }

    //Show user how many upper or lower case charters there was
    printf("\nThere was : %d lower case letters and : %d upper case\n", lower , upper ) ;

    cons = 0 ;

    //Again loop for running trough the arrays
    for(j = 0 ; j <= 10 ; j ++ ) {


        if( word[j] == 'a' || word[j] == 'e' || word[j] == 'i' || word[j] == 'o' || word[j] == 'u' ) {
        cons ++ ;
        }
    }

    for(j = 0 ; j <= 10 ; j ++ ) {

        if( word[j] == 'A' || word[j] == 'E' || word[j] == 'I' || word[j] == 'O' || word[j] =='U' ) {
        cons ++ ;
        }
    }



    printf("There is : %d vowels on your letters.", cons ) ;



    return 0;
}
