/* Program description*/
/*----------------------
Program: cylinder height calculator
Programmer  
Student ID: 
Date: 22.09.17
Purpose: Calculate the height of cylinder based on volume and radius then check answer.
-------------------------*/

#include <stdio.h>
#include <stdlib.h>


//read in volume and radius to get height

int main(void)
{   
    float radius = 0 , height = 0 , volume = 0 , volume_answer = 0;
    const float pie = 3.1416 ;

    //askins the radius from user
    printf("Hello let me calculate the heigh of your cylinder\n");
    printf("Please enter the radius of the cylinder : \n" ) ;
    scanf("%f", &radius ) ;
    // asking volume from user
    printf("Please enter the volume of the cylinder : " ) ;
    scanf("%f" , &volume ) ;

    //Calculating and printing
    height = volume / ( pie * ( radius * radius )  ) ;
    printf("The height will be : %.3f \n" , height ) ;

    //Reverse calculating
    volume_answer = pie * (radius * radius) * height ;
    printf("let's checkc the answer!\n" ) ;
    printf("if height is : %.3f and radius is %.3f then volume will be %.3f :" , height , radius ,volume_answer ) ;

    return 0;
}
