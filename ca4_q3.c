/* Program description*/
/*----------------------
Program: three in reverse
Programmer  
Student ID: 
Date: 22.09.17
Purpose: ask user 3 digits long integer number and reverse it
-------------------------*/


#include <stdio.h>
#include <stdlib.h>

int main(void)
{   //decalre vars for later use
    int number = 0, remainder = 0 , reversed = 0 , n = 0;
    //asking user to enter numbers and limitin it to 3 digits
    printf("Hello! Plese enter three digits long integer number for reversing : \n" ) ;
    scanf("%3d" , &number ) ;

    //startin while loop while user entered number is not 0 and loop will make it 0 to reverse it
    while( number != 0 )  {
    //take modulus or what is left after 10
    remainder = number % 10 ;
    //use reversed variable to hold value of reversed. in first loop this will be just remainder multiplied by 10
    reversed =  reversed * 10 + remainder ;
    //on each loop divide the number by 10 to get it smaller towards nd
    number /= 10;
    }
    //print out the reversed number
    printf("The number in reverse is %d" , reversed ) ;
    return 0;
}

