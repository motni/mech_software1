/* Program description*/
/*----------------------
Program: Stamp duty calculator
Programmer  
Student ID: 
Date: 21.10.17
Purpose: Shows to user how much the stamp duty is and how much is it in money
-------------------------*/



#include <stdio.h>
#include <stdlib.h>


int main(void)
{
    //declare the property value variable
    float value ;

    //ask the user for property  value
    printf("Hello let's calcualte how much goverment stamp duty you need to pay from your property \n" ) ;
    printf("Please enter your property value in euros : \n" ) ;
    scanf("%f", &value ) ;

    //show to user what they did enter
    printf("you did enter %.2f \n", value) ;

    //Start the if and if else chain to to show user how much is it
    if(value < 100000) {
        printf("your stamp duty is 2%c and it is : %.2f in euros\n", 37 , value * 0.02 ) ;
    }
    else if( value < 150000) {
        printf("Your stamp duty is 5%c and it is : %.2f in euros\n", 37, value * 0.05 ) ;
    }
    else if( value < 200000) {
        printf("Your stamp duty is 10%c and it is : %.2f in euros\n", 37, value * 0.10 ) ;
    }
    else

     {
        printf("Your stamp duty is 12.5%c and it is : %.2f  in euros\n", 37, value * 0.125 ) ;
    }
    //else NULL ;

    return 0;
}
