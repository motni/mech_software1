/* Program description*/
/*----------------------
Program: Hello world console print.
Programmer  
Student ID: 
Date: 22.09.17
Purpose: Print hello world with student details to console.
-------------------------*/

#include <stdio.h>
#include <stdlib.h>

int main()
{
    printf("Hello world! My name is  Foo bar and my student ID is \n");
    return 0;
}
