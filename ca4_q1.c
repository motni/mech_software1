/* Program description*/
/*----------------------
Program: Days in month
Programmer  
Student ID: 
Date: 12.10.17
Purpose: Ask month number from user and print how many days there is in it
-------------------------*/

#include<stdio.h>



/* Program to demonstrate switch and case */
void main()
{   //decalre int for month number
    int month = 0;
    //ask the month from user
    printf("Please enter a month number: \n" ) ;
    scanf("%d", &month ) ;

    //start switch condition and loop it trough all months
    switch(month)
    {
    case 2 :
    printf("FEB has 28 days" );
        break ;
    case 1 :
    printf("January has 31 days\n");
        break ;
    case 3 :
    printf("March has 31 days\n");
        break ;
    case 5 :
    printf("May has 31 days\n");
        break ;
    case 7 :
    printf("July has 31 days\n");
        break ;
    case 8 :
    printf("August has 31 days\n");
        break ;
    case 10 :
    printf("October has 31 days\n");
        break ;
    case 11 :
    printf("November has 31 days\n");
        break ;
    case 12 :
    printf("December has 31 days\n");
        break ;
    case 4 :
    printf("April has 30 days\n");
        break ;
    case 6 :
    printf("June has 31 days\n");
        break ;
    case 9 :
    printf("Septempber has 31 days\n");
        break ;
    //end the swithc loop by defaulting it to "error"
    default :
        printf("No such month!\n");
    }
}
