/* Program description*/
/*----------------------
Program: Triange perimerter and area
Programmer  
Student ID: 
Date: 18.11.17
Purpose: Calculate triangles area and perimeter useing x and y coordinates
-------------------------*/


#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main()
{
    //declare lot's of floats for calculations
    float first[2], second[2], third[2], side1, side2, side3, p, area ;

    //Ask the x and y coordinates from user as x and y pairs
    printf("Hello world. Let's calculate the perimeter and area of your triangle\n");
    printf("Please enter x and y values of the triangle coordinates\n") ;

    printf("The first set of x and y values : \n ") ;
    scanf("%f %f", &first[0], &first[1] ) ;

    printf("The second set of x and y values : \n ") ;
    scanf("%f %f", &second[0], &second[1] ) ;

    printf("The third set of x and y values : \n ") ;
    scanf("%f %f", &third[0], &third[1] ) ;

    //start the maths
    //Calculate how long are the sides coordinates are in form A's X is first[0] and A's Y is first[1]
    // B is second C is third
    side1 = sqrt( (pow( (second[0] - first[0]), 2 )) + (pow((second[1] - first[1]), 2 ))   ) ;

    side2 = sqrt( (pow( (third[0] - second[0]), 2 )) + (pow((third[1] - second[1]), 2 ))   ) ;

    side3 = sqrt( (pow( (third[0] - first[0]), 2 )) + (pow((third[1] - first[1]), 2 ))   ) ;

    //Show the user what we got
    printf("First side is : %f long and second is : %f last one is : %f \n " , side1, side2, side3 ) ;
    printf("and sum of them is : %f \n", side1 + side2 + side3 ) ;


    /*Heron's formula comes from :
    https://www.mathopenref.com/heronsformula.html
    */

    //Calcualte the area and show it to user
    p = (side1+side2+side3)/2 ;
    area = sqrt((p * ( p - side1 ) * ( p - side2 ) * ( p -side3 ) )) ;
    printf("The area of triangel is : %f using heron's formula" , area) ;


    return 0;
}
