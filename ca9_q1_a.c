/* Program description*/
/*----------------------
Program: Biggest number
Programmer  
Student ID: 
Date: 25.11.17
Purpose: User inputs 3 numbers and program will tell which one is the biggest
-------------------------*/


#include <stdio.h>
#include <stdlib.h>

//Declare the function prototype
int max(int x, int y,  int z ) ;


int main()
{
    //Declare the variables to be used in this
    int x = 0,   y = 0,  z = 0 ;

    //Ask the numbers from user
    printf("Hello world. Let's see which one of your 3 numbers is max ");
    printf("Please enter 3 numbers : ") ;
    scanf("%d,", &x ) ;
    scanf("%d,", &y ) ;
    scanf("%d,", &z ) ;


    //Print out from the function what number was biggest
    printf("Biggest number is : %d", max(x, y, z) ) ;


    return 0;
}

    //Start the function with user given variables
    int max(int x, int y,  int z )  {
        if(x >= y && x >= z ) {
            return x ;
        }
        else if(y >= x && y >= z ) {
            return y ;
        }
        else if (z >= y && z >= x ) {
            return z ;
        }
    }
