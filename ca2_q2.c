/* Program description*/
/*----------------------
Program: 1.1 to 2.0 mean
Programmer  
Student ID: 
Date: 30.10.17
Purpose: The program will sum the floating poing numbers from 1.1 to 2.0 and show the average of them.
-------------------------*/

#include <stdio.h>
#include <stdlib.h>

int main(void)
{   //for loop for numbers 3 to 8
    //declare the variables for start
    int a,sum1,count = 0 ;
    float b = 0,sum2 = 0, mean = 0;
    //create the loop
    for( a = 3; a <= 8; a++)
    //sum the loop items
    sum1 += a;
    //print out the sum
    printf("The sum of numbers 3 to 8 is : %d\n",sum1);


    // start the loop for float numbers 1.1 to 2.0
    for(b = 1.1; b <= 2.0; b += .1)
    //sum the loop
    sum2 += b;
    // get the mean by dividing the sum with integer.
    mean = sum2 /10;
    //print it out
    printf("Mean of floating point numbers 1.1 to 2 are : %f\n",mean);


    //Create whole lot of variables
    float c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,sum3,mean3 = 0;
    //assign values to them one by one
    c1 = 1.1;
    c2 = 1.2;
    c3 = 1.3;
    c4 = 1.4;
    c5 = 1.5;
    c6 = 1.6;
    c7 = 1.7;
    c8 = 1.8;
    c9 = 1.9;
    c10 = 2.0;
    //sum it up
    sum3 = c1+c2+c3+c4+c5+c6+c7+c8+c9+c10;
    //divide the sum with n items to get mean
    mean3 = sum3 / 10;
    //print out the mean
    printf("Hard way mean for floats 1.1 to 2.0 is %f,",mean3);
    return 0;
}
