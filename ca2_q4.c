/* Program description*/
/*----------------------
Program: Income to bills
Programmer  
Student ID: 
Date: 22.09.17
Purpose: Print out how many paper bills and coins one's income would be
-------------------------*/

#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    //Declaring notes and coins variables.
    int bill100,bill200,bill50,bill20,bill10,bill5,coin2,coin1 = 0  ;
    //Declaring variables for remainders
    int remainder200,remainder100,remainder50,remainder20,remainder10,remainder5,remainder2,remainder1 = 0   ;
    int long income = 0 ;
    // asking the income from user
    printf("Let's see how many bills and coins your salar will be\n Please enter your income per annum?\n");
    scanf("%d",&income);

    //Dividing the income with 200 and then taking the modulus.
    bill200 = income / 200 ;
    remainder200 = income % 200 ;
    printf("there would be : %d\t200euro bills and : %d euros left.\n", bill200, remainder200 ) ;

    //Continuing to make calcualtions whats left for 100 euro bills.
    bill100 = remainder200 / 100 ;
    remainder100 = remainder200 % 100 ;
    printf("%d,\t100euro bills and : %d euros remaining.\n", bill100, remainder100 ) ;

    //Going down the ladder to 50 euro bills.
    bill50 = remainder100 / 50 ;
    remainder50 = remainder100 % 50 ;
    printf("%d,\t50euro bills and : %d euros left.\n", bill50, remainder50 ) ;

    //Going down the ladder to 20 euro bills.
    bill20 = remainder50 / 20 ;
    remainder20 = remainder50 % 20 ;
    printf("%d,\t20euro bills and : %d euros left.\n", bill20, remainder20 ) ;

    //Down the ladder to 10 euro bills.
    bill10 = remainder20 / 10 ;
    remainder10 = remainder20 % 10 ;
    printf("%d,\t10euro bills and : %d euros left\n", bill10, remainder10 ) ;

    //down to 5 euro bills.
    bill5 = remainder10 / 5 ;
    remainder5 = remainder10 % 5 ;
    printf("%d,\t5euro bills and : %d euros left.\n", bill5, remainder5 ) ;

    //down to 2 euro coing
    coin2 = remainder5 / 2 ;
    remainder2 = remainder5 % 2 ;
    printf("%d,\t2euro coins and : %d euros left.\n", coin2, remainder2 ) ;

    //down to 1 euro coin.
    coin1 = remainder2 / 1 ;
    remainder1 = remainder2 % 1 ;
    printf("%d,\t1euro coins and : %d euros left.\n", coin1, remainder1 ) ;

    return 0;
}
