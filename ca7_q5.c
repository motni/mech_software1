/* Program description*/
/*----------------------
Program: Multipcaltion tables
Programmer  
Student ID: 
Date: 7.12.17
Purpose: Print out multiplication tables
-------------------------*/

#include <stdio.h>
#include <stdlib.h>

int main() {

    int number = 0, i = 0, j = 0, k = 1, sum = 0, n = 0 ;

    printf("Hello world. Let's do some multipclation tables \n");
    printf("Please enter number you want to see the table for : ") ;
    scanf("%d" , &number  ) ;

    i = 1 ;
    j = 1 ;
    n = 1 ;

    for(k = 1 ; k <= number ; k ++ ) {

        while(i <= number ) {

            printf("%d \t", i) ;

                for(n = 1 ; n <= number ; n ++ ) {

                    while(j <= number ) {

                        printf("  %d   ", j * i  ) ;
                        j ++ ;

                    }
        }
        printf("\n") ;
        i ++ ;
        j = 1 ;

    }
    }


    return 0;
}
