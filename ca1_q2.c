/* Program description*/
/*----------------------
Program: Printing things
Programmer 
Student ID: 
Date: 22.09.17
Purpose: Print some marks to console
-------------------------*/


#include <stdio.h>
#include <stdlib.h>

int main()
{
    printf("   *******  	*******	  *******     * \n");
    printf("\n");
    printf("  * * * * *     *     *      *       *** \n");
    printf("\n");
    printf("   *******      *     *      *      ***** \n");
    printf("\n");
    printf("  * * * * *     *     *      *	      * \n");
    printf("\n");
    printf("   ******       *******   *******     * \n");

    return 0;
}
