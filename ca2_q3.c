/* Program description*/
/*----------------------
Program: 2 integer number math operator
Programmer  
Student ID: 
Date: 22.09.17
Purpose: Program will take 2 numbers from user and perform few pasic mathematical operations with them.
-------------------------*/

#include <stdio.h>
#include <stdlib.h>

int main()
{   //Declare input integer variables.
    int integ1,integ2 = 0;
    //Declare math operation output variables.
    int diffrence,product,modulus = 0;
    //Notfying user that were asking for numbers then scanf to get the numbers.
    printf("Hello, I'm going to ask 2 numbers from you and then perform mathematical operations with them.\n");
    printf("Please enter the first number :\n");
    scanf("%d",&integ1);
    //asking for second number.
    printf("Please enter the second number :\n");
    scanf("%d",&integ2);
    //Printing out the numbers user did enter.
    printf("You did enter %d and %d\n",integ1,integ2);
    //Performing the math operations.
    diffrence = integ1 - integ2;
    product = integ1 * integ2;
    modulus = integ1  % integ2;
    //printing out what the math operations did do to the numbers
    printf("The diffrence of numbers is : %d and the product is %d\n finally the modulus of the numbers is : %d",diffrence,product,modulus);
    return 0;
}
