/* Program description*/
/*----------------------
Program: Statistics calculator
Programmer  
Student ID: 
Date: 22.09.17
Purpose: Print out few statistical calculations from user entered 10 numbers
-------------------------*/




#include <stdio.h>
#include <stdlib.h>
#include <math.h>


char main()
{
    //declare array of ints
    int array[10];
    //couple int variables for calculations
    int loop = 0, count = 0 ;
    //floating point number for the standard deviation
    float  stdev = 0, mean = 0, sum = 0 ;
    //floats for upper and lower limits
    float ulimit = 0 , llimit = 0 ;

    //get the numbers in by scanning them in loop
    printf("Please enter ten integer numbers\n");
    for(loop=0 ; loop<10 ; loop++) {
    scanf("%d", &array[loop]);
    }

    /*sum them up with by looping them to plus eguals thingy
    Usually loops steps are i but I'm using loop, make it clear I have written it. */
    for(loop = 0; loop < 10; loop++) {
    printf("%d ", array[loop]);

    //use casting to sum them up as sum is float
    sum += (float)array[loop] ;
    }

    //Print out what we got as sum
    printf("The sum of the numbers is : %f \n" , sum ) ;

    //Calculate the mean
    mean = sum / 10 ;

    //print out the mean
    printf("mean is : %f \n", mean ) ;

    /*starting to calculate the standard deviation
    make the sum of square by looping*/

    for(loop = 0 ; loop < 10 ; loop ++ ) {
    stdev += pow((array[loop] - mean) , 2 ) ;
    }

    //Print out the standard deviation and finish the calculation on same line as well
    printf("the stdev is : %.2f \n", sqrt(stdev / 9) ) ;

    /*I also need to calculate the standard deviation squared over n-1 to the actual variable so I can use it later on for
    upper and lower values I could do this before and just print it out but let's be fancy with the print out line */

    stdev = sqrt(stdev / 9 ) ;
    //upper limit
    ulimit = mean + ( 3 * stdev ) ;
    llimit = mean - ( 3 * stdev ) ;

    //print out the upper and lower
    printf("The upper limit is : %.2f and the lower limit is %.2f  \n", ulimit, llimit ) ;

    //getting fancy with the return value
    return 'ok' ;
}
