/* Program description*/
/*----------------------
Program: Body mass index calculator
Programmer  
Student ID: 
Date: 22.09.17
Purpose: Asks for users weight and heigt then calucaltes BMI
-------------------------*/


#include <stdio.h>
#include <stdlib.h>

int main(void) {
    // Declare variables for all we need
    int unsigned weight = 0, bmi = 0 ;
    float height = 0 ;

    //Print to user what we are about to do and ask the user height.
    printf("Hello, Let's see if you Body mass index BMI is over 18 \nPlease enter you height in meters : " ) ;
    scanf("%f", &height ) ;

    //Ask user weight.
    printf("Then please enter your weight in kilograms : " ) ;
    scanf("%d",&weight);

    //Assign the BMI caluclation to bmi variable
    bmi = weight / ( height * height )  ;

    //Prtin out the BMI
    printf("Your BMI is : %d", bmi ) ;
    return 0;
}
