/* Program description*/
/*----------------------
Program: Cube volume
Programmer  
Student ID: 
Date: 22.09.17
Purpose: Print cubes volume on the console in cubic centimeters
-------------------------*/

#include <stdio.h>
#include <stdlib.h>

int main(VOID)
{
    float x,y,z,volume,surface1,surface2,surface3,sumsurface; 
    x = 12;
    y = 11.6;
    z = 2.7;

    //calculate the volume and but it variable
    volume = x * y * z;
    surface1 = x * z * 2;
    surface2 = x * y * 2;
    surface3 = y * z * 2;
    sumsurface = surface1 + surface2 + surface3;
    printf("Hello the volume of 12cm * 11.6cm * 2.7cm is : %.2f cubic centimeters\n", volume);
    printf("the total surface area of the cube is : %f square centimeters ", sumsurface ) ;

    return 0;
}
