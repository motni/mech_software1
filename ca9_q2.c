/* Program description*/
/*----------------------
Program: Print users name to a text file
Programmer  
Student ID: 
Date: 26.11.17
Purpose: Print users name to a text file
-------------------------*/


#include <stdio.h>
#include <stdlib.h>

int main() {

    //String arrays
    char first[12] ;
    char last[12] ;
    char fromfile[26] ;


    //pointer to a open file
    FILE *name ;



    //Ask the users name
    printf("Hello world. Let's write your first name and last name to a text file\n" ) ;
    printf("Please enter your first name :\n") ;
    scanf("%s" , &first ) ;
    printf("Please enter your surname : \n" ) ;
    scanf("%s" , &last ) ;

    //print out the names for error checking
    printf("%s,  %s", first, last ) ;


    //Assign variable name to the open file
    name = fopen("name.txt", "w") ;

    //Check that we can open the file in write mode
    if(name != NULL ) {
    printf("\nAll good file is open for writing. \n");

    //If we can open the file and write the name in there
    fprintf(name, "%s ", first );
    fprintf(name, "%s", last );
    fclose(name ) ;
    }

    //if we can't open the file print out error
    else  {
    printf("Error can't open the file. \n") ;
    }

    printf("Ok let's read the file then Hit enter to continue : \n");

    //Then let's read the file to console

    //Assign file open in read mode for the variable
    name = fopen("name.txt", "r") ;

    //If the name is not null it's there
    if(name != NULL ) {
    //
    printf("All good file is open for reading. \n");
    fscanf( name, "%[^\n]", fromfile ) ;
    printf("%s", fromfile ) ;
    }
    else  {
    printf("Error can't open the file. \n") ;
    }

    return 0;
}
